module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageReporters: [
    "json",
    "lcov",
    "text",
    "cobertura"
  ],
  collectCoverageFrom: [
    'src/**/*.ts',
    '!src/index.ts'
  ]
}
