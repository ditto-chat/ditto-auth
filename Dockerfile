# Dockerfile to build the ditto-auth docker images.
#
# To build the image, run `docker build` command from the root of the
# ditto-auth repository:
#
#    docker build .
#

###
### Stage 0: builder
###
FROM node:12-alpine as builder

WORKDIR /install

COPY . /install

RUN yarn && yarn build


###
### Stage 1: runtime
###

FROM node:12-alpine

ENV NODE_ENV=production

WORKDIR /ditto-auth

COPY --from=builder /install/dist/* /install/package.json /install/yarn.lock /ditto-auth/

RUN yarn

VOLUME ["/config"]

EXPOSE 8080

ENTRYPOINT ["node", "index.js"]
CMD ["--config", "/config/config.json"]