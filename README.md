# Ditto Auth Gateway
[![Version](https://img.shields.io/badge/dynamic/json?color=820db6&label=version&query=%24%5B0%5D.tag_name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Fditto-chat%252Fditto-auth%2Freleases)](https://gitlab.com/ditto-chat/ditto-auth/-/releases)
[![License - Apache 2.0](https://img.shields.io/badge/license-Apache%202.0-352b60)](https://gitlab.com/ditto-chat/ditto-auth/blob/master/LICENSE)
[![coverage report](https://gitlab.com/ditto-chat/ditto-auth/badges/dev/coverage.svg)](https://gitlab.com/ditto-chat/ditto-auth/-/commits/dev)
[![Discover - Ditto](https://img.shields.io/badge/discover-ditto-820db6)](https://gitlab.com/ditto-chat/ditto-mobile/)

This is a simple authentication gateway written as a Node module in TypeScript. The goal is to secure the transmission of an [SSO login token](https://matrix.org/docs/spec/client_server/latest#sso-client-login) between a [Matrix](https://matrix.org/) homeserver and a mobile App to avoid interception of the token by a malicious app due to the limitations of deep linking. This is based on the [PKCE flow](https://tools.ietf.org/html/rfc7636).


## Setup

1. Clone this repository
2. Rename `config.json.sample` to `config.json` and change parameters if needed
3. Run `yarn up`
4. The gateway should be listening at `http://localhost:8080/` unless you changed the port


## Docker container

### Using Docker CLI

```shell
# Build the image
docker build -t ditto-auth-local .

# Start (change the path to config.json, and the port if needed)
docker run --name ditto-auth \
           -p 8080:8080 \
           --mount type=bind,src=/path/to/config.json,dst=/config/config.json,ro \
	       ditto-auth-local
```

### Using Docker Compose

Change the port and path to config.json in docker-compose.yml if needed, then:

```shell
# Build the image
docker-compose build

# Start
docker-compose up
```

## Usage

This container is based on the PKCE flow so every step has to happen in that order:

### 1. Generate the verifier and the challenge

The `verifier` can be a string of any form or any length. A UUID is a good choice.

The `challenge` is the SHA256 sum of the `verifier` as an hex string

### 2. Register the transaction

Send a `POST` request to the `/register` endpoint with a JSON body that provides the `challenge` and the `url` corresponding to the [homeserver SSO redirect url](https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-login-sso-redirect) with the `redirectUrl` query parameter corresponding to the encoded URL of the gateway.

The response body is a JSON object with the `txn` key which is a base64 url-safe random transaction ID.

#### Example request

```http
POST /register HTTP/1.1
```
Body:
```json
{
    "challenge": "410964bf9cca840537ad475a50a71d860494107ee8fb6d2536f5b98425f18879",
    "url": "https://homeserver/_matrix/client/r0/login/sso/redirect?redirectUrl=https%3A%2F%2Fauth-gateway%2F"
}
```

#### Example response

```http
HTTP/1.1 200 OK
```
Body:
```
{
    "txn": "7GvK54AIQ-4DmjOOMuqc5Q"
}
```

### 3. Redirect the user to the SSO login

Open the `/redirect` endpoint with the `txn` as a query parameter in a browser and the user will be redirected to the homeserver SSO login page provided during registration. The `url` has been augmented with the proper endpoint and `txn`.

#### Example request

```http
GET /redirect?txn=7GvK54AIQ-4DmjOOMuqc5Q HTTP/1.1
```

#### Example response

```http
HTTP/1.1 302 Found
Location: https://homeserver/_matrix/client/r0/login/sso/redirect?redirectUrl=https%3A%2F%2Fauth-gateway%2Flogin%3Ftxn%3D7GvK54AIQ-4DmjOOMuqc5Q
```

### 4. Redirect the user to the app

After the SSO login is completed, the user is redirected to the `/login` endpoint with the `txn` and the `loginToken`. They are then redirected to the mobile app thanks to deep linking, using the `appURL` provided in `config.json`. The app receives the `txn` as a query parameter.

#### Example request

```http
GET /login?txn=7GvK54AIQ-4DmjOOMuqc5Q&loginToken=187de07460a5ec548f4517b775609c6ad37bde56906e1e450fb6b45d33689816 HTTP/1.1
```

#### Example response

```http
HTTP/1.1 302 Found
Location: dittochat://login/sso?txn=7GvK54AIQ-4DmjOOMuqc5Q
```

### 5. Resolve the transaction

Send back a JSON object with the `txn` and the `verifier` to the `/resolve` endpoint. If the `verifier` matches the stored `challenge`, the response will be another JSON object with the `ssoToken` key corresponding to the `loginToken`. It can then be used to login to the homeserver with an `m.login.token`.

#### Example request

```http
POST /resolve HTTP/1.1
```
Body:
```json
{
    "txn": "7GvK54AIQ-4DmjOOMuqc5Q",
    "verifier": "this-looks-like-a-uuid"
}
```

#### Example response

```http
HTTP/1.1 200 OK
```
Body:
```
{
    "ssoToken": "187de07460a5ec548f4517b775609c6ad37bde56906e1e450fb6b45d33689816"
}
```

### Validity of the `txn`

- The transaction is deleted as soon as it is resolved.
- The transactions are deleted 30 minutes after creation if they're not resolved. This avoids to have a lot of stale data.
- The transactions are stored in memory, so they don't persist between gateway reboots.

### Errors

When the parameters are invalid or missing, the gateway responds with a `400` and tries to have a helpful response.
```http
HTTP/1.1 400 Bad Request
```

When the `verifier` does not match the `challenge` at the `/resolve` endpoint, the gateway returns a `403`.
```http
HTTP/1.1 403 Forbidden
```

All the other errors will return a `404`.
```http
HTTP/1.1 404 Forbidden
```