import fs from 'fs'
import yargs from 'yargs'

import Server, { ServerConfig } from './server'
import transactions from './transactions'

const configFile = yargs.options({
  config: { type: 'string', default: './config.json' }
}).argv.config
console.info('Loading config from file', configFile)

const config: ServerConfig = JSON.parse(fs.readFileSync(configFile, 'utf8'))

const server = new Server(config)
const port = server.getPort()

server.getApp().listen(port, (): void => {
  console.log(`Server is listening on port ${port}`)

  // Clean transactions every 5 minutes
  setInterval((): void => {
    transactions.prune()
  }, 5 * 60 * 1000)
})
