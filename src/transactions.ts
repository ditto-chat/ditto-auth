import crypto from 'crypto'

interface Transaction {
  challenge: string
  url: string
  token?: string
  created: number
}

class Transactions {
  private readonly store: Map<string, Transaction>

  constructor () {
    this.store = new Map()
  }

  register (challenge: string, url: string): string {
    let txn = this.random()
    let test = this.store.get(txn)

    while (typeof test !== 'undefined') {
      txn = this.random()
      test = this.store.get(txn)
    }

    const created = Date.now()
    this.store.set(txn, { challenge, url, created })
    return txn
  }

  redirect (txn: string): string | undefined {
    const url = this.store.get(txn)?.url
    if (typeof url === 'undefined') return
    return url + encodeURIComponent(`login?txn=${txn}`)
  }

  login (txn: string, token: string): string | undefined {
    const transaction = this.store.get(txn)
    if (typeof transaction === 'undefined') return
    transaction.token = token
    return txn
  }

  resolve (txn: string, verifier: string): string | undefined {
    const transaction = this.store.get(txn)
    if (typeof transaction === 'undefined') return

    const hash = crypto.createHash('sha256').update(verifier).digest('hex')
    if (hash !== transaction.challenge) return ''

    this.store.delete(txn)
    return transaction.token
  }

  prune (): void {
    // Delete transactions older than 30 minutes
    const thirtyMinAgo = Date.now() - (30 * 60 * 1000)
    for (const [txn, transaction] of this.store.entries()) {
      if (transaction.created < thirtyMinAgo) {
        this.store.delete(txn)
      }
    }
  }

  private random (): string {
    const rand = crypto.randomBytes(16).toString('base64')
    const noplusslash = rand.replace(/[+/]/g, '-')
    const noequal = noplusslash.replace(/=/g, '')
    return noequal
  }
}

const transactions = new Transactions()
export default transactions
