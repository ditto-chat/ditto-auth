import express from 'express'

import transactions from './transactions'

enum ServerError {
  Unknown,
  Invalid,
  WrongVerifier
}

export interface ServerConfig {
  port: number
  appUrl: string
}

export default class Server {
  #app: express.Express
  #config: ServerConfig

  constructor (config: ServerConfig) {
    this.#config = config

    this.#app = express()
    this.#app.use(express.json())

    this.#app.post('/:action', (req, res) => {
      const { action } = req.params

      switch (action) {
        case 'register': {
          const { challenge, url } = req.body
          if (typeof challenge !== 'string' || typeof url !== 'string') {
            this.sendError(res, ServerError.Invalid, 'missing challenge or url')
          } else {
            const txn = transactions.register(challenge, url)
            console.debug(`Transaction ${txn} registered`)
            res.json({ txn })
          }
          break
        }
        case 'resolve': {
          const { txn, verifier } = req.body
          if (typeof txn !== 'string' || typeof verifier !== 'string') {
            this.sendError(res, ServerError.Invalid, 'missing txn or verifier')
          } else {
            const ssoToken = transactions.resolve(txn, verifier)
            if (typeof ssoToken === 'undefined') this.sendError(res)
            else if (ssoToken === '') this.sendError(res, ServerError.WrongVerifier)
            else {
              console.debug(`Transaction ${txn} resolved`)
              res.json({ ssoToken })
            }
          }
          break
        }
        default:
          this.sendError(res)
      }
    })

    this.#app.get('/:action', (req, res) => {
      const { action } = req.params

      switch (action) {
        case 'redirect': {
          const { txn } = req.query
          if (typeof txn !== 'string') this.showError(res, ServerError.Invalid)
          else {
            const url = transactions.redirect(txn)
            if (typeof url === 'undefined') this.showError(res)
            else res.redirect(url)
          }
          break
        }
        case 'login': {
          const { loginToken, txn } = req.query
          if (typeof txn !== 'string' || typeof loginToken !== 'string') {
            this.showError(res, ServerError.Invalid)
          } else {
            const login = transactions.login(txn, loginToken)
            if (typeof login === 'undefined') this.showError(res)
            res.redirect(`${this.#config.appUrl}?txn=${txn}`)
          }
          break
        }
        default:
          this.showError(res)
      }
    })

    this.#app.post('/', (req, res) => {
      this.sendError(res)
    })

    this.#app.get('/', (req, res) => {
      this.showError(res)
    })
  }

  getApp (): express.Express {
    return this.#app
  }

  getPort (): number {
    return this.#config.port
  }

  private sendError (res: express.Response, errcode?: ServerError, reason?: string): void {
    switch (errcode) {
      case ServerError.Invalid: {
        res.status(400).json({
          errcode: 'INVALID',
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          error: `Invalid request: ${reason}`
        })
        break
      }
      case ServerError.WrongVerifier:
        res.status(403).json({
          errcode: 'FORBIDDEN',
          error: 'The verifier and the txn do not match'
        })
        break
      default:
        res.status(404).json({
          errcode: 'NOT_FOUND',
          error: 'Nothing here'
        })
    }
  }

  private showError (res: express.Response, errcode?: ServerError): void {
    switch (errcode) {
      case ServerError.Invalid:
        res.status(400).send('400: Bad request')
        break
      default:
        res.status(404).send('404: Page not found')
    }
  }
}
