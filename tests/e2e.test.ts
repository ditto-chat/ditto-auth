import request from 'supertest'

import Server from '../src/server'

const server = new Server({ appUrl: 'scheme-e2e://login/sso', port: 1234 })

describe('given valid data', () => {
  const temp = { txn: '' }

  test('POST /register returns a valid txn', async () => {
    const response = await request(server.getApp())
      .post('/register')
      .send({
        challenge: '6b26469ba3865368bd2467e5640e7d5e34a0daae98ceb37fc811648ef119f315',
        url: 'https://url-valid-e2e/'
      })

    temp.txn = response.body.txn

    expect(response.status).toBe(200)
    expect(temp.txn).toMatch(/^[\w-]+$/)
  })

  test('GET /redirect redirects to proper url', async () => {
    const response = await request(server.getApp())
      .get(`/redirect?txn=${temp.txn}`)

    expect(response.status).toBe(302)
    expect(response.header.location).toBe(`https://url-valid-e2e/login%3Ftxn%3D${temp.txn}`)
  })

  test('GET /login redirects to proper url', async () => {
    const response = await request(server.getApp())
      .get(`/login?txn=${temp.txn}&loginToken=token_valid_e2e`)

    expect(response.status).toBe(302)
    expect(response.header.location).toBe(`scheme-e2e://login/sso?txn=${temp.txn}`)
  })

  test('POST /resolve returns token', async () => {
    const response = await request(server.getApp())
      .post('/resolve')
      .send({
        txn: temp.txn,
        verifier: '1d0727ae-63ea-4ba6-b1b6-75a6b464a07a'
      })

    expect(response.status).toBe(200)
    expect(response.body.ssoToken).toBe('token_valid_e2e')
  })

  test('transaction has been deleted so GET /redirect returns 404', async () => {
    const response = await request(server.getApp())
      .get(`/redirect?txn=${temp.txn}`)

    expect(response.status).toBe(404)
  })
})

describe('given malformed data', () => {
  test('POST /register returns 400', async () => {
    const response1 = await request(server.getApp())
      .post('/register')
      .send({ challenge: 'useless' })
    const response2 = await request(server.getApp())
      .post('/register')
      .send({ url: 'useless' })
    const response3 = await request(server.getApp())
      .post('/register')
      .send({})
    const response4 = await request(server.getApp())
      .post('/register')

    expect(response1.status).toBe(400)
    expect(response2.status).toBe(400)
    expect(response3.status).toBe(400)
    expect(response4.status).toBe(400)
  })

  test('GET /redirect returns 400', async () => {
    const response = await request(server.getApp())
      .get('/redirect')

    expect(response.status).toBe(400)
  })

  test('GET /login returns 400', async () => {
    const response1 = await request(server.getApp())
      .get('/login?txn=useless')
    const response2 = await request(server.getApp())
      .get('/login?loginToken=useless')
    const response3 = await request(server.getApp())
      .get('/login')

    expect(response1.status).toBe(400)
    expect(response2.status).toBe(400)
    expect(response3.status).toBe(400)
  })

  test('POST /resolve returns 400', async () => {
    const response1 = await request(server.getApp())
      .post('/resolve')
      .send({ txn: 'useless' })
    const response2 = await request(server.getApp())
      .post('/resolve')
      .send({ verifier: 'useless' })
    const response3 = await request(server.getApp())
      .post('/resolve')
      .send({})
    const response4 = await request(server.getApp())
      .post('/resolve')

    expect(response1.status).toBe(400)
    expect(response2.status).toBe(400)
    expect(response3.status).toBe(400)
    expect(response4.status).toBe(400)
  })
})

describe('given invalid data', () => {
  test('GET /redirect returns 404 if txn is invalid', async () => {
    const response = await request(server.getApp())
      .get('/redirect?txn=invalid_txn_1!')

    expect(response.status).toBe(404)
  })

  test('GET /login returns 404 if txn is invalid', async () => {
    const response = await request(server.getApp())
      .get('/login?txn=invalid_txn_2!&loginToken=useless')

    expect(response.status).toBe(404)
  })

  test('POST /resolve returns 404 when txn is invalid', async () => {
    const response = await request(server.getApp())
      .post('/resolve')
      .send({ txn: 'invalid_txn_3!', verifier: 'useless' })

    expect(response.status).toBe(404)
  })

  test('POST /resolve returns 403 when [challenge, verifier] tuple is invalid', async () => {
    const responseRegister = await request(server.getApp())
      .post('/register')
      .send({ challenge: 'challenge_invalid!', url: 'useless' })
    const txn: string = responseRegister.body.txn

    expect(responseRegister.status).toBe(200)
    expect(txn).toMatch(/^[\w-]+$/)

    const responseLogin = await request(server.getApp())
      .get(`/login?txn=${txn}&loginToken=useless`)

    expect(responseLogin.status).toBe(302)

    const responseResolve = await request(server.getApp())
      .post('/resolve')
      .send({ txn, verifier: 'verifier_invalid!' })

    expect(responseResolve.status).toBe(403)
  })
})

describe('accessing wrong endpoints or valid endpoints with wrong method', () => {
  test('GET /register returns 404', async () => {
    const response = await request(server.getApp())
      .get('/register')

    expect(response.status).toBe(404)
  })

  test('POST /redirect returns 404', async () => {
    const response = await request(server.getApp())
      .post('/redirect')

    expect(response.status).toBe(404)
  })

  test('POST /login returns 404', async () => {
    const response = await request(server.getApp())
      .post('/login?txn=useless')

    expect(response.status).toBe(404)
  })

  test('POST /resolve returns 404', async () => {
    const response = await request(server.getApp())
      .get('/resolve')

    expect(response.status).toBe(404)
  })

  test('GET / returns 404', async () => {
    const response = await request(server.getApp())
      .get('/')

    expect(response.status).toBe(404)
  })

  test('POST / returns 404', async () => {
    const response = await request(server.getApp())
      .post('/')

    expect(response.status).toBe(404)
  })
})
