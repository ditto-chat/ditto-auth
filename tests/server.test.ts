import request from 'supertest'

import Server from '../src/server'
import transactions from '../src/transactions'

const mockTransactions = {
  register: jest.spyOn(transactions, 'register'),
  redirect: jest.spyOn(transactions, 'redirect'),
  login: jest.spyOn(transactions, 'login'),
  resolve: jest.spyOn(transactions, 'resolve')
}

describe('given valid data', () => {
  const server = new Server({ appUrl: 'scheme_valid://login/sso', port: 1111 })

  test('POST /register returns txn', async () => {
    mockTransactions.register.mockClear()
    mockTransactions.register.mockReturnValueOnce('txn_valid_register')

    const response = await request(server.getApp())
      .post('/register')
      .send({ challenge: 'useless', url: 'useless' })

    expect(mockTransactions.register.mock.calls).toHaveLength(1)
    expect(response.status).toBe(200)
    expect(response.body.txn).toBe('txn_valid_register')
  })

  test('GET /redirect returns url', async () => {
    mockTransactions.redirect.mockClear()
    mockTransactions.redirect.mockReturnValueOnce('https://url-valid-redirect/')

    const response = await request(server.getApp())
      .get('/redirect?txn=useless')

    expect(mockTransactions.redirect.mock.calls).toHaveLength(1)
    expect(response.status).toBe(302)
    expect(response.header.location).toBe('https://url-valid-redirect/')
  })

  test('GET /login returns scheme', async () => {
    mockTransactions.login.mockClear()
    mockTransactions.login.mockReturnValueOnce('useless')

    const response = await request(server.getApp())
      .get('/login?txn=txn_valid_login&loginToken=useless')

    expect(mockTransactions.login.mock.calls).toHaveLength(1)
    expect(response.status).toBe(302)
    expect(response.header.location).toBe('scheme_valid://login/sso?txn=txn_valid_login')
  })

  test('POST /resolve returns token', async () => {
    mockTransactions.resolve.mockClear()
    mockTransactions.resolve.mockReturnValueOnce('token_resolve_valid')

    const response = await request(server.getApp())
      .post('/resolve')
      .send({ txn: 'useless', verifier: 'useless' })

    expect(mockTransactions.resolve.mock.calls).toHaveLength(1)
    expect(response.status).toBe(200)
    expect(response.body.ssoToken).toBe('token_resolve_valid')
  })
})

describe('given malformed data', () => {
  const server = new Server({ appUrl: 'scheme_invalid://login/sso', port: 2222 })

  test('POST /register returns 400', async () => {
    mockTransactions.register.mockClear()

    const response1 = await request(server.getApp())
      .post('/register')
      .send({ challenge: 'useless' })
    const response2 = await request(server.getApp())
      .post('/register')
      .send({ url: 'useless' })
    const response3 = await request(server.getApp())
      .post('/register')
      .send({})
    const response4 = await request(server.getApp())
      .post('/register')

    expect(mockTransactions.register.mock.calls).toHaveLength(0)
    expect(response1.status).toBe(400)
    expect(response2.status).toBe(400)
    expect(response3.status).toBe(400)
    expect(response4.status).toBe(400)
  })

  test('GET /redirect returns 400', async () => {
    mockTransactions.redirect.mockClear()

    const response = await request(server.getApp())
      .get('/redirect')

    expect(mockTransactions.redirect.mock.calls).toHaveLength(0)
    expect(response.status).toBe(400)
  })

  test('GET /login returns 400', async () => {
    mockTransactions.login.mockClear()

    const response1 = await request(server.getApp())
      .get('/login?txn=useless')
    const response2 = await request(server.getApp())
      .get('/login?loginToken=useless')
    const response3 = await request(server.getApp())
      .get('/login')

    expect(mockTransactions.login.mock.calls).toHaveLength(0)
    expect(response1.status).toBe(400)
    expect(response2.status).toBe(400)
    expect(response3.status).toBe(400)
  })

  test('POST /resolve returns 400', async () => {
    mockTransactions.resolve.mockClear()

    const response1 = await request(server.getApp())
      .post('/resolve')
      .send({ txn: 'useless' })
    const response2 = await request(server.getApp())
      .post('/resolve')
      .send({ verifier: 'useless' })
    const response3 = await request(server.getApp())
      .post('/resolve')
      .send({})
    const response4 = await request(server.getApp())
      .post('/resolve')

    expect(mockTransactions.resolve.mock.calls).toHaveLength(0)
    expect(response1.status).toBe(400)
    expect(response2.status).toBe(400)
    expect(response3.status).toBe(400)
    expect(response4.status).toBe(400)
  })
})

describe('if data was not found in transactions', () => {
  const server = new Server({ appUrl: 'scheme_no_transactions://login/sso', port: 3333 })

  test('GET /redirect returns 404 when transaction is undefined', async () => {
    mockTransactions.redirect.mockClear()
    mockTransactions.redirect.mockReturnValueOnce(undefined)

    const response = await request(server.getApp())
      .get('/redirect?txn=useless')

    expect(mockTransactions.redirect.mock.calls).toHaveLength(1)
    expect(response.status).toBe(404)
  })

  test('GET /login returns 404 when transaction is undefined', async () => {
    mockTransactions.login.mockClear()
    mockTransactions.login.mockReturnValueOnce(undefined)

    const response = await request(server.getApp())
      .get('/login?txn=useless&loginToken=useless')

    expect(mockTransactions.login.mock.calls).toHaveLength(1)
    expect(response.status).toBe(404)
  })

  test('POST /resolve returns 404 when transaction is undefined', async () => {
    mockTransactions.resolve.mockClear()
    mockTransactions.resolve.mockReturnValueOnce(undefined)

    const response = await request(server.getApp())
      .post('/resolve')
      .send({ txn: 'useless', verifier: 'useless' })

    expect(mockTransactions.resolve.mock.calls).toHaveLength(1)
    expect(response.status).toBe(404)
  })

  test('POST /resolve returns 403 when token is invalid', async () => {
    mockTransactions.resolve.mockClear()
    mockTransactions.resolve.mockReturnValueOnce('')

    const response = await request(server.getApp())
      .post('/resolve')
      .send({ txn: 'useless', verifier: 'useless' })

    expect(mockTransactions.resolve.mock.calls).toHaveLength(1)
    expect(response.status).toBe(403)
  })
})

describe('accessing wrong endpoints or valid endpoints with wrong method', () => {
  const server = new Server({ appUrl: 'scheme_invalid://login/sso', port: 4444 })

  test('GET /register returns 404', async () => {
    const response = await request(server.getApp())
      .get('/register')

    expect(response.status).toBe(404)
  })

  test('POST /redirect returns 404', async () => {
    const response = await request(server.getApp())
      .post('/redirect')

    expect(response.status).toBe(404)
  })

  test('POST /login returns 404', async () => {
    const response = await request(server.getApp())
      .post('/login?txn=useless')

    expect(response.status).toBe(404)
  })

  test('POST /resolve returns 404', async () => {
    const response = await request(server.getApp())
      .get('/resolve')

    expect(response.status).toBe(404)
  })

  test('GET / returns 404', async () => {
    const response = await request(server.getApp())
      .get('/')

    expect(response.status).toBe(404)
  })

  test('POST / returns 404', async () => {
    const response = await request(server.getApp())
      .post('/')

    expect(response.status).toBe(404)
  })
})

describe('given config', () => {
  test('login redirects to proper url', async () => {
    mockTransactions.login.mockReturnValueOnce('useless')

    const server = new Server({ appUrl: 'scheme_config://login/sso', port: 5555 })
    const response = await request(server.getApp())
      .get('/login?txn=txn_config&loginToken=useless')

    expect(response.status).toBe(302)
    expect(response.header.location).toBe('scheme_config://login/sso?txn=txn_config')
  })

  test('getPort() returns proper port', async () => {
    const server = new Server({ appUrl: 'scheme_config://login/sso', port: 6666 })
    const port = server.getPort()

    expect(port).toBe(6666)
  })
})
