import crypto from 'crypto'

import transactions from '../src/transactions'

const randomBytes = jest.spyOn(crypto, 'randomBytes')
const dateNow = jest.spyOn(Date, 'now')

describe('given valid data', () => {
  const temp = { txn: '' }

  randomBytes.mockImplementation(() => {
    return Buffer.from('zjGF+VpWBAH3lyk/Kzj8ow==', 'base64')
  })

  test('register returns a url-safe string', () => {
    temp.txn = transactions.register(
      'c6c82016738c7add8ae6af75ebc295d5f4e6db1a7b27bfdd5d355b1408dc89c7',
      'https:/url-valid/'
    )

    expect(temp.txn).toBe('zjGF-VpWBAH3lyk-Kzj8ow')
  })

  test('redirect returns the proper url', () => {
    expect(transactions.redirect(temp.txn))
      .toBe(`https:/url-valid/login%3Ftxn%3D${temp.txn}`)
  })

  test('login and resolve return the token', () => {
    transactions.login(temp.txn, 'token_valid')

    expect(transactions.resolve(temp.txn, 'bac7e5e5-1471-4b4a-9c6e-a3e8974b054b'))
      .toBe('token_valid')
  })

  test('redirect now returns undefined', () => {
    expect(transactions.redirect(temp.txn)).toBeUndefined()
  })
})

describe('given invalid data', () => {
  test('redirect returns undefined if invalid txn', () => {
    expect(transactions.redirect('txn_invalid_redirect')).toBeUndefined()
  })

  test('login returns undefined if invalid txn', () => {
    expect(transactions.login('txn_invalid_login', 'useless')).toBeUndefined()
  })

  test('resolve returns undefined if invalid txn', () => {
    expect(transactions.resolve('txn', 'useless')).toBeUndefined()
  })

  test('resolve returns empty string if invalid verifier', () => {
    const txn = transactions.register('challenge_invalid', 'useless')
    transactions.login(txn, 'useless')
    expect(transactions.resolve(txn, 'verifier_invalid')).toBe('')
  })
})

describe('given duplicate random values', () => {
  test('register returns different txn', () => {
    randomBytes.mockClear()
    randomBytes.mockImplementationOnce(() => Buffer.from('same string'))
      .mockImplementationOnce(() => Buffer.from('same string'))
      .mockImplementationOnce(() => Buffer.from('different string'))

    const temp = {
      txn1: '',
      txn2: ''
    }

    temp.txn1 = transactions.register('useless', 'useless')
    temp.txn2 = transactions.register('useless', 'useless')

    expect(temp.txn1).not.toBe(temp.txn2)
    expect(randomBytes.mock.calls).toHaveLength(3)

    randomBytes.mockRestore()
  })
})

describe('clean up', () => {
  const temp = { txn: '', timestamp: 0 }

  test('after register, prune does nothing so redirect returns a url', () => {
    temp.txn = transactions.register('useless', 'https://url-cleanup/')
    temp.timestamp = Date.now()
    transactions.prune()

    expect(transactions.redirect(temp.txn)).toBe(`https://url-cleanup/login%3Ftxn%3D${temp.txn}`)
  })

  test('30 minutes later, prune deletes transaction so redirect does not return a url', () => {
    dateNow.mockClear()
    dateNow.mockImplementationOnce(() => (temp.timestamp + (31 * 60 * 1000)))

    transactions.prune()
    expect(dateNow.mock.calls).toHaveLength(1)
    expect(transactions.redirect(temp.txn)).toBeUndefined()
  })
})
