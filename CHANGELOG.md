# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security


## [v1.0.0] - 2020-05-13

First release of the gateway


[Unreleased]: https://gitlab.com/ditto-chat/ditto-mobile/compare/1.0.0...dev
[v1.0.0]: https://gitlab.com/ditto-chat/ditto-mobile/-/tags/1.0.0
